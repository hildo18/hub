#language: pt
Funcionalidade:
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação

    @cadastro
    Cenario: Fazer cadastro
        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulario de cadastro:
            | nome         | email            | senha  |
            | Pamela Silva | pamela@teste.com | 131826 |
        Então sou redirecionado para o Dashboard

    Esquema do Cenário:Tentativa de cadastro
        Dado que acesso a página de cadastro

        Quando submeto o seguinte formulario de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta:"<mensagem_output>"

        Exemplos:
            | nome_input   | email_input       | senha_input | mensagem_output                   |
            |              | pamelax@teste.com | 131826     | Oops. Informe seu nome completo! |
            | Pamela Silva |                   | 131826      | Oops. Informe um email válido!    |
            | Pamela Silva | pamelax.teste.com | 31826      | Oops. Informe um email válido!   |
            | Pamela Silva | pamela#teste.com  | 31826       | Oops. Informe um email válido!    |
            | Pamela Silva | pamelax@teste.com |            | Oops. Informe sua senha secreta! |

