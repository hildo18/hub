#language: pt 
@anuncio
Funcionalidade: Cadastro de anuncios
Sendo usuario cadastrado no Rocklov que possui equipamento musical
Quero cadastrar meus equipamentos
Para que eu possa disponibilizados para locaçao
Cenário: Novo equipamento

Dado que estou logado como "pamela@teste.com" e "131826"
E que acesso o formulario de cadastro de anuncios
E que eu tenho o seguinte equipamento:
        |thumb     |fender-sb.jpg    |
        |Nome      |Fender Strato  |
        |Categoria |Cordas         |
        |Preco     |200            |
Quando submeto o cadastro desse item
Então devo ver esse item no meu Dashboard
