#language: pt
Funcionalidade:
Sendo um usuário cadastrado
Quero acessar o sistema da Rocklov
Para que eu possa anunciar meus equipamentos musicais

@login
Cenario: Login do usuário

    Dado que acesso a pagina principal
    Quando submeto minhas credenciais com "pamela@teste.com" e "131826"
    Então sou redirecionado para o Dashboard
    
@tentativa
Esquema do Cenario: Tentar Logar

    Dado que acesso a pagina principal
    Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
    Então vejo a mensagem de alerta:"<mensagem_output>"

    Exemplos:
        |email_input     |senha_input |mensagem_output                 |
        |pamelax@teste.com|111         |Usuário e/ou senha inválidos.   |
        |pamela%teste.com|131826      |Oops. Informe um email válido!  |
        |pamela1teste.com|131826      |Oops. Informe um email válido!  |
        |                |131826      |Oops. Informe um email válido!  |
        |pamelax@teste.com|            |Oops. Informe sua senha secreta!|
